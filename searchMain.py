import random
from search import linear_search
from search import binary_search
from time import time
import matplotlib.pyplot as plt

# Finding a particular element in Linear Search
data = random.sample(range(5000), 5000)
index = linear_search(data, data[4000])
print(index)

# Finding a particular element in Binary Search
data = random.sample(range(7000), 7000)
index = linear_search(data, data[6900])
print(index)

n = 10000
linearTimeArr = []
binaryTimeArr = []
sizeArr = []

# Linear Search Execution Time 
for i in range(n,n*11,n):
	sizeArr.append(i)
	randomvalues = random.sample(range(i), i)
	startTime = time()
	linear_search(randomvalues, randomvalues[i-1])
	endTime = time()
	elapsedTime = endTime - startTime
	linearTimeArr.append(elapsedTime)
	print("Linear search execution time for last value in size",i,"is",elapsedTime)

# Binary Search Execution Time
for i in range(n,n*11,n):
	randomvalues = random.sample(range(i), i)
	startTime = time()
	randomvalues.sort()
	binary_search(randomvalues, randomvalues[i-1])
	endTime = time()
	elapsedTime = endTime - startTime
	binaryTimeArr.append(elapsedTime)
	print("Binary search for last value in size",i,"is",elapsedTime)

# Plot input-size vs execution-time graph
fig, ax = plt.subplots(1, 1)
ax.plot(sizeArr,linearTimeArr, label = 'Linear Search')
ax.plot(sizeArr,binaryTimeArr, label = 'Binary Search')
legend = ax.legend(loc = 'upper center', fontsize = 'large')
plt.show()