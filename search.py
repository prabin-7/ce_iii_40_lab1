
data = []

def linear_search(data, target):
    for i in range(len(data)):
        if data[i] == target:
            return i

    return -1

def binary_search(data, target):

    low = 0
    high = len(data) - 1

    while low <= high:
      
        mid = (low + high)//2
       
        if data[mid] == target:
            return mid
        elif data[mid] > target:
            high = mid - 1
        else:
            low = mid + 1

    return -1   

if __name__=="__main__":
	print("Input numbers to add to list and any other character to finish")
	# To take only integer values
	while True:
		try:
			x=input("Input Number:")
			x=int(x)
			data.append(x)
		except:
			break

	print(data)
	searchVal = int(input("Input the values to be searched from the above list: "))

	linSearchVal = linear_search(data,searchVal)

	if(linSearchVal == -1):
		print("The value",searchVal,"doesn't exist in the list")
	else:
		print("The value lies in ",linSearchVal,"according to Linear Search")

	data.sort()
	binSearchVal = binary_search(data,searchVal)
	
	if(binSearchVal==-1):
		print("The value",searchVal,"doesn't exist in the list")
	else:
		print("The value lies in ",binSearchVal,"according to Binary Search in sorted data")
 