import unittest
from search import linear_search
from search import binary_search

class TestSearch(unittest.TestCase):
# Linear Search Test for integers
    def test_linear_search(self):
        data = [7, 2, 4, 9, 0, 11, 15, 19, 27]
        self.assertEqual(linear_search(data, 19), 7)
        self.assertEqual(linear_search(data, 0), 4)
        self.assertEqual(linear_search(data, 10), -1)

# Linear Search Test for characters
    def test_searchChar(self):
        data = ['t', 'a', 'b', 'l', 'e']
        self.assertEqual(linear_search(data, 'a'), 1)

# Binary Search Test
    def test_binary_search(self):
        #Sorted dataay for Binary Search
        data = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.assertEqual(binary_search(data, 3), 2)
        self.assertEqual(binary_search(data, 5), 4)
        self.assertEqual(binary_search(data, 1), 0)
        self.assertEqual(binary_search(data, 11), -1)

if __name__ == "__main__":
    unittest.main()
    # All the tests will run with no errors as the index at which the values are searched are correct
	# If self.assertEqual(linear_search(data, 3), 2) is done in test_search(self) then test case will fail
    # Likewise if self.assertEqual(binary_search(data, 10), 2) is done in test_search(self) then test case will fail
