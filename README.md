# CE_III_40_Lab1

**Purpose:** Implementation, testing and performance measurement of linear search and binary search algorithms.

**Tasks**
1. Implement linear and binary search algorithms.
2. Write some test cases to test your program.
3. Generate some random inputs for your program and apply both linear and binary search algorithms to find a particular element on the generated input. Record the execution times of both algorithms for best and worst cases on inputs of different size (e.g. from 10000 to 100000 with step size as 10000). Plot an input-size vs execution-time graph.
4. Explain the observations.

**Descriptions**
1. The linear and binary search algorithms are implemented in the search.py file.
2. unittest, a Python unit testing framework, is used to write test case for both search algorithms in searchTest.py file.
3. In searchMain.py file, random module is used to generate some random inputs for the program. Then, both linear and binary search algorithms are applied to find a particular element on the generated input after importing linear_search and binary_search from search.py file. After that time module is imported to record the execution times for both algorithms for best and worst cases on inputs of sizes from 10000 to 100000 with stepsize as 10000. At last, an input-size vs execution-time graph is plotted.
4. The graph for the linear search algorithm varies linearly with time. Since the binary search algorithm should include the sorting part for the searching, its graph varies differently than that of linear search. Since the worst case time complexity for binary search is O(log n) and that for linear search is only O(n), the plotted graph shows the rapid increase in execution time for binary search when compared to binary search in the same step-sizes. Thus the graph complies with the nature of time complexity of respective search algorithms. Since best case time complexity for both linear search and binary search is O(1) so the graph of input-size vs execution-time is not plotted for best case scenario. 